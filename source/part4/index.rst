.. _part4:

*************************************************************************************************
Partie 4 | Code Generation
*************************************************************************************************

JVM Bytecode proposed by Group 26, Christophe Crochet and Antoine Rime
==========================================================


1. Some theory:
""""""""""""""

1. Why is Java languague machine is named Bytecode ? Explain it briefly
2. It exists different types of machine language. Stack, RISC and CISC. What is the one used by the JVM.
3. Give one advantage of Stack machine compare to RISC architecture.

2. Some Translation:
"""""""""""""""""""

1. Write the Bytecode for this function
2. Give the stack for the the input a = 2

pubic static int inc(int a)
{
  int b = 0;

  for (int i=0;i<a;i++)
  {
    b++;
  }
  return b;
}


Bytecode proposed by Group 20, Romain Hainaut, Robin Hormaux
===================================================================

1. Translate the following Java code into his bytecode.
"""""""""""""""""""""""""""""""""""""""""""""""""""""""

public static int compute (int a, int b) {
    while (b != 0) {
        if (a > b) {
            a = a - b;
        }
        else {
            b = b - a;
        }
    }
    return a;
}


2. Given the following bytecode:
""""""""""""""""""""""""""""""""

public static int compute (int n) {
0: iconst_0
1: istore_2
2: iconst_1
3: istore_3
4: iload_1
5: if_icmple 20
8: iload_2
9: istore_4
10: iload_3
11: istore_2
12: iload_3
13: iload_4
14: iadd
15: istore_3
16: iinc 1 1
17: goto 4
20: iload_2
21: ireturn
}

1. Write a corresponding Java code. What does it compute?
2. Execute it for parameters n=5. Illustrate the operand stack and local variables after each step.


Answers:
======================================
1. Theory
""""""""""""""

1. Each instruction is encoded with a fixed number of bytes. For example the imul instruction is encoded as 0110 1000 in the output file.
2. The JVM is a stack machine since it use a stack and each operation operate on it.
3. One advantage of the Stack machine is that, for one instruction on a RISC architecture with 32 registers you need 32*31=992 different opcodes wheras for Stack machine you would only need one.

2. Exercises
"""""""""""""""
Bytecode::

> 0 iconst_0
> 1 istore_2
> 2 iconst_0
> 3 istore_3
> 4 iload_3
> 5 iload_1
> 6 if_icmpge 15
> 9 iinc 2 1
> 12 iinc 3 1
> 15 goto 6
> 16 iload_3
> 17 ireturn
